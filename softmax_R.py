import warnings
import math
def fxn():
	warnings.warn("deprecated" ,DeprecationWarning)

with warnings.catch_warnings():
	warnings.simplefilter("ignore")
	fxn()

import matplotlib.pyplot as plt

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)
import tensorflow as tf

x = tf.placeholder(tf.float32, [None, 784])
prt_x = tf.placeholder(tf.float32,[None])
W = tf.Variable(tf.zeros([784, 10]))
b = tf.Variable(tf.zeros([10]))

y = tf.nn.softmax(tf.matmul(x, W) + b)

y_ = tf.placeholder(tf.float32, [None, 10])

cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

sess = tf.InteractiveSession()

tf.global_variables_initializer().run()
for _ in range(1000):
	batch_xs, batch_ys = mnist.train.next_batch(100)
	sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))



accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
import numpy as np

yonX=np.linspace(start=1,stop=10000,num=10000)

maxy=tf.argmax(y,1)

maxy_=tf.argmax(y_,1)

import pandas as pd

freq = np.full(10000,1,dtype=int)
my=sess.run(maxy, feed_dict={x:mnist.test.images})
my_=sess.run(maxy_,feed_dict={y_: mnist.test.labels})
df = pd.DataFrame({"Actual":my_,"predicted":my,"f":freq})

table = pd.pivot_table(df,index=['Actual','predicted'],values='f',aggfunc=np.sum)

fin=table.reset_index()

plt.scatter(fin.iloc[:,0],fin.iloc[:,1],s=fin.iloc[:,-1])
plt.title('Actual image number vs Predicted image number')
plt.savefig('plot')
plt.show()
